﻿function openNav(event) {
    document.getElementById("mySidenav").style.width = "30%";
    document.getElementById("main").style.marginLeft = "30%";
    document.body.style.backgroundColor = "rgba(0, 0, 0, .5)";
}

function openAddPropertyNav(event) {
    document.getElementById("sideNavAddProperty").style.width = "30%";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.body.style.backgroundColor = "white";
}

function closeAddPropertyNav() {
    document.getElementById("sideNavAddProperty").style.width = "0";
}

function buildPropertiesList(propertiesList) {
    var classPropertiesList = $("#classPropertiesList");
    $.each(propertiesList, function (value, index) {
        $(classPropertiesList).append("<label for='property" + index + "'>" + value.name + "</label>")
                              .append(" <input type='text' class='form-control' id='property" + index + "' value='" + value.name + "'>")
    });
}

function test() {
    $('.visibility-toggle-div').animate({ height: "toggle", opacity: "toggle" }, "slow");
}

