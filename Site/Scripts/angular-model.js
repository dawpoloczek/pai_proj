﻿var app = angular.module('projectPAIApp', ["ngRoute"]);

app.config(function ($routeProvider, $locationProvider) {

    $routeProvider.when("/login", {
        templateUrl: "Views/Login.html",
        controller: "loginCtrl",
        publicAccess: true,
    });

    $routeProvider.when("/classes", {
        templateUrl: "Views/Classes.html",
        controller: "classesCtrl"
    });

    $routeProvider.otherwise({ redirectTo: '/login' });
    $locationProvider.html5Mode(true);
});

app.service('sharedProperties', function () {
    var canEdit = false;

    return {
        getCanEdit: function () {
            return canEdit;
        },
        setCanEdit: function (value) {
            canEdit = value;
        }
    };
});

app.controller('classesCtrl', function ($rootScope, $scope, $location, $http, sharedProperties) {
    var getClassesUrl = "http://localhost:5000/pai/api/v1.0/clsses";
    var getClassUrl = "http://localhost:5000/pai/api/v1.0/clsses/";
    var getClassPropertiesUrl = "http://localhost:5000/pai/api/v1.0/classProperties/";
    var updateClassProperties = "http://localhost:5000/pai/api/v1.0/updateClass";
    var panellumViewer;

    var initPanellum = function (imageName) {
        panellumViewer = pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": "../Sources/Images/Classes/" + imageName
        });
    };

    var destroyPanellum = function () {
        panellumViewer.destroy();
    };

    $scope.canEdit = $rootScope.logged;
    $scope.isInEdit = false;
    $scope.classes = [];
    $scope.currentClassNumber = {};
    $scope.newProperty = { name: "", value: "" };
    $scope.currentClass = {};
    $scope.currentClassProperties = [];
    $scope.classPropertiesToRemove = [];

    $scope.getClassProperties = function (number) {
        $scope.currentClassNumber = number;
        var getUrl = getClassPropertiesUrl + number;
        $http.get(getUrl).then(function successCallback(response) {
            $scope.currentClassProperties = response.data.class_properties;
            $scope.classPropertiesToRemove = [];
        }, function errorCallback(response) {
            var x = 0;
        });
    };

    $scope.changeCurrentClass = function (number) {
        $scope.currentClassNumber = number;
        var getUrl = getClassUrl + number;
        $http.get(getUrl).then(function successCallback(response) {
            $scope.currentClassProperties = response.data.class_properties;
            $scope.classPropertiesToRemove = [];
            $scope.currentClass = response.data.class;
            openNav();
            $('.visibility-toggle-div').animate({ height: "toggle", opacity: "toggle" }, "slow");
            initPanellum($scope.currentClass["imageName"]);
        }, function errorCallback(response) {
            var x = 0;
        });
    };

    $scope.addProperty = function () {
        var element = {
            "propertyName": $scope.newProperty["name"],
            "propertyValue": $scope.newProperty["value"]
        };
        $scope.newProperty = { "name": "", "value": "" };
        $scope.currentClassProperties.push(element);
        closeAddPropertyNav();
    }

    $scope.cancelEditClassProperties = function () {
        $scope.isInEdit = false;
        $scope.getClassProperties($scope.currentClassNumber);
    }

    $scope.closeNavBar = function () {
        $('.visibility-toggle-div').animate({ height: "toggle", opacity: "toggle" }, "slow");
        destroyPanellum();
        $scope.isInEdit = false;
        closeNav();
    }

    $scope.removeClassProperty = function (index) {
        var propertyToRemove = $scope.currentClassProperties[index];
        $scope.classPropertiesToRemove.push(propertyToRemove);
        $scope.currentClassProperties.splice(index, 1);
    }

    $scope.logout = function () {
        $rootScope.logged = false;
        $location.path("/login");
    };

    $scope.saveClassProperties = function () {
        var parameter = angular.toJson({ classObject: $scope.currentClass, classProperties: $scope.currentClassProperties, propertiesToRemove: $scope.classPropertiesToRemove });
        $http.post(updateClassProperties, parameter).
        then(function (data, status, headers, config) {
            $scope.getClassProperties($scope.currentClassNumber);
            console.log(data);
        }).catch(function (data, status, headers, config) {
            console.log(data);
        });
        $scope.isInEdit = false;
    }

    //$http.get(getClassesUrl).then(function successCallback(response) {
    //    $scope.classes = response.data.classes;
    //}, function errorCallback(response) {
    //    var x = 0;
    //});
});

app.controller('loginCtrl', function ($rootScope, $scope, $location, $http, sharedProperties) {
    var loginAsAdminUrl = "http://localhost:5000/pai/api/v1.0/login";

    $rootScope.logged = false;
    $scope.loginName = "";
    $scope.loginPassword = "";

    $scope.login = function () {
        var parameter = angular.toJson({ loginName: this.loginName, userPassword: this.loginPassword });
        $http.post(loginAsAdminUrl, parameter).then(function (data, status, headers, config) {
            if (data.status == 200) {
                $rootScope.logged = true;
                $location.path("/classes");
            }
            console.log(data);
        }).catch(function (data, status, headers, config) {
            console.log(data);
        });
    };

    $scope.goAsGuest = function () {
        $rootScope.logged = false;
        $location.path("/classes");
    };
});

//app.run(function ($rootScope, $location) {
//    $rootScope.$on('$routeChangeStart', function () {
//        if (!$rootScope.logged) {
//            console.log('DENY');
//            //event.preventDefault();
//            $location.path("/login");
//        }
//    });
//});