import json
from flask import Flask, jsonify
from MongoBDConnector import MongoBDConnector
from flask import request, Response
from JSONEncoder import JSONEncoder
from flask_cors import CORS, cross_origin

test = MongoBDConnector('localhost', 27017)

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return "Hello, World!"

@app.route('/pai/api/v1.0/clsses', methods=['GET'])
def get_classes():
    classes = test.GetClassesList();
    return JSONEncoder().encode({'classes': classes})

@app.route('/pai/api/v1.0/classProperties/<int:class_number>', methods=['GET'])
def get_casssProperties(class_number):
    class_properties = test.GetClassPropertiesList(class_number);
    return JSONEncoder().encode({'class_properties': class_properties})

@app.route('/pai/api/v1.0/clsses/<int:class_number>', methods=['GET'])
def get_classAndProperties(class_number):
    class_properties = test.GetClassPropertiesList(class_number);
    found_class = test.GetClassByNumber(class_number);
    return JSONEncoder().encode({'class_properties': class_properties, "class" : found_class})

@app.route('/pai/api/v1.0/updateClass', methods=["POST"])
def update_classProperties():
    data = json.loads(request.data)
    test.AddClassProperties(data["classObject"], data["classProperties"]);
    test.RemoveClassProperties(data["classObject"], data["propertiesToRemove"]);
    return "OK"

@app.route('/pai/api/v1.0/login', methods=["POST"])
def loginAdmin():
    data = json.loads(request.data)
    login = data["loginName"]
    password = data["userPassword"]
    response = ""
    if login == "admin":
        if password == "admin":
            response = "OK"
        else:
            response = Response('Bledne haslo', 401, {'WWWAuthenticate':'Basic realm="Login Required"'})
    else:
        response = Response('Bledna nazwa uzytkownika', 401, {'WWWAuthenticate':'Basic realm="Login Required"'})
    return response;

if __name__ == '__main__':
    app.run(debug=True)