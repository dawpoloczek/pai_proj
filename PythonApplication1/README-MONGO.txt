MongoDB:

1. Pobieramy Mongo w wersji community ze strony:
https://www.mongodb.com/download-center?jmp=nav#community

2. Instalacja zgodnie z dokumentacj�:
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/#manually-create-a-windows-service-for-mongodb-community-edition

W skr�cie:
Mongo potrzebuje folderu w kt�rym mo�e przechowywa� dane. Standardowo jest to: \data\db (ten folder trzeba samemu stworzy�).
Je�li chcemy przechowywa� dane w innym folderze mamy dwie opcje:
	- zmieni� wpis w pliku \etc\mongod.conf (pozycja storage.dbPath)
	- odpala� serwer podaj�c jawnie lokalizacj� folderu z danymi:  $ mongod --dbpath "�cie�ka do foldery z danymi"

Serwer odpalamy poprzez polecenie $ mongod [--dbpath "�cie�ka do foldery z danymi"] (konsol� trzeba zostawi� otwart�, po jej zamkni�ciu wy��cza si� serwer)
Odpalenie klienta w trybie interaktywnym: $ mongo

I to wszystko

3. Zabawa z baz�: https://docs.mongodb.com/manual/reference/method/

4. Tworzymy baz� o nazwie PAI_DB z dwoma kolekcjami (odpowiednik tabeli z baz relacyjntch, elementy tabeli to dokumenty): Classes, ClassProperties. Schematy objekt�w s� nast�puj�ce:
   (przygotuj� potem skrypty tworz�ce te struktury). Og�lnie nie ma �adnych obostrze� co do tego, jakie w�a�ciwo�ci b�d� mia�y poszczeg�lne elementy kolekcji.
   Mo�na dodawa� dokumenty o r�nych w�a�ciwo�ciach. Trzeba si� jednak trzyma� jakiej� konwencji, �eby m�c potem w normalny spos�b przeszukiwa� kolekcje.


Class:
	{number: ,
     description: ,
	 name:	}

ClassProperty:
	{
		classId: ,
		propertyName: ,
		propertyValue: ,
		propertyType: 
	}