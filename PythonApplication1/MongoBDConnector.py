import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId

class MongoBDConnector(object):
    
    def __init__(self, host, port):
        self.__mdb_host = host
        self.__mdb_port = port
        self.__mdb_client = None
        self.__mdb_database = None
        return

    def __getDatabase(self):
        if self.__mdb_database is None:
            self.__mdb_client = MongoClient(self.__mdb_host, self.__mdb_port)
            self.__mdb_database = self.__mdb_client.PAI_DB
        return self.__mdb_database

    def GetClassByNumber(self, classNumber):
        db = self.__getDatabase();
        foundClass = db.Classes.find_one({'number': classNumber})
        return foundClass

    def GetClassesCursor(self):
        db = self.__getDatabase();
        classes = db.Classes.find()
        return classes

    def GetClassesList(self):
        db = self.__getDatabase();
        classes = list(self.GetClassesCursor())        
        return classes

    def GetClassPropertiesCursor(self, classNumber):
        db = self.__getDatabase();
        classObject = self.GetClassByNumber(classNumber)
        foundProperties = db.ClassProperties.find({'classId': classObject['_id']})
        return foundProperties

    def GetClassPropertiesList(self, classNumber):
        db = self.__getDatabase();
        foundProperties = list(self.GetClassPropertiesCursor(classNumber))
        return foundProperties

    def AddClass(self, number, name, description):
        db = self.__getDatabase();
        db.Classes.insert_one(object)
        return

    def AddClass(self, object):
        db = self.__getDatabase();
        db.Classes.insert_one(object)
        return

    def AddClassProperty(self, classObject, property):
        db = self.__getDatabase();
        property['classId'] = ObjectId(classObject['_id'])
        if property.has_key('_id'):
            property['_id'] = ObjectId(property['_id'])
            db.ClassProperties.update( { "_id": ObjectId(property['_id'])}, property, upsert=True )
        else:
            db.ClassProperties.insert_one(property)     
        return
    
    def removeClassProperty(self, classObject, property):
        db = self.__getDatabase();
        property['classId'] = ObjectId(classObject['_id'])
        if property.has_key('_id'):
            property['_id'] = ObjectId(property['_id'])
            db.ClassProperties.delete_one( { "_id": ObjectId(property['_id'])})
        return

    def AddClassProperties(self, classObject, properties):
        db = self.__getDatabase();
        for element in properties:
            self.AddClassProperty(classObject, element)
        return

    def RemoveClassProperties(self, classObject, properties):
        db = self.__getDatabase();
        for element in properties:
            self.removeClassProperty(classObject, element)
        return
        



